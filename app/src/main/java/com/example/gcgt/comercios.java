package com.example.gcgt;

public class comercios {
    private String cod_comercio;
    private String title;
    private String oferta;
    private String direccion;
    private String correo;
    private String tipo;
    private String tel1;
    private String tel2;
    private String w1;
    private String t1;
    private String w2;
    private String t2;
    private String h1;
    private String h2;
    private String h3;
    private String h4;
    private String alt;
    private String lng;
    private String urlimg;



    public comercios(String cod_comercio, String title, String oferta, String direccion, String correo, String tipo, String tel1, String tel2, String w1, String t1, String w2, String t2, String h1, String h2, String h3, String h4, String alt, String lng, String urlimg){
        this.cod_comercio = cod_comercio;
        this.title = title;
        this.oferta = oferta;
        this.direccion = direccion;
        this.correo = correo;
        this.tipo = tipo;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.w1 = w1;
        this.t1 = t1;
        this.w2 = w2;
        this.t2 = t2;
        this.h1 = h1;
        this.h2 = h2;
        this.h3 = h3;
        this.h4 = h4;
        this.alt = alt;
        this.lng = lng;
        this.urlimg = urlimg;
    }

    public String getCod_comercio(){ return cod_comercio; }

    public String getTitle() {
        return title;
    }

    public String getOferta() {
        return oferta;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public String getTipo() {
        return tipo;
    }

    public String getTel1() {
        return tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public String getW1() {
        return w1;
    }

    public String getT1() {
        return t1;
    }

    public String getW2() {
        return w2;
    }

    public String getT2() {
        return t2;
    }

    public String getH1() {
        return h1;
    }

    public String getH2() {
        return h2;
    }

    public String getH3() {
        return h3;
    }

    public String getH4() {
        return h4;
    }

    public String getAlt() {
        return alt;
    }

    public String getLng() {
        return lng;
    }

    public String getUrl(){return urlimg;}
}
