package com.example.gcgt;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class popup extends Activity {

    TextView instalar, cancelar;

    public static String PLAYSTORE_FACE_URL = "https://play.google.com/store/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popup);

        instalar = findViewById(R.id.textView6);
        cancelar = findViewById(R.id.textView7);

        instalar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent playstoreIntent = new Intent(Intent.ACTION_VIEW);
                String playurl = getFacebookPlayURL(getApplicationContext());
                playstoreIntent.setData(Uri.parse(playurl));
                startActivity(playstoreIntent);
                System.exit(0);
            }
        });

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.9),(int)(height*.3));
    }

    public String getFacebookPlayURL(Context context) {
        return PLAYSTORE_FACE_URL;
    }
}
