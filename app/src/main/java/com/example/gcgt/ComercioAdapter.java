package com.example.gcgt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ComercioAdapter extends RecyclerView.Adapter<ComercioAdapter.ComercioViewHolder> {

    private Context mCtx;

    //we are storing all the products in a list
    private List<comercios> productList;

    //getting the context and product list with constructor
    public ComercioAdapter(Context mCtx, List<comercios> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ComercioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        return new ComercioViewHolder();
    }

    @Override
    public void onBindViewHolder(ComercioViewHolder holder, final int position) {
        final comercios comer = productList.get(position);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCtx, infowindow.class);
                Bundle b1 = new Bundle();
                intent.putExtras(b1);
                mCtx.startActivity(intent);
            }
        });
    }

    public void filterList(ArrayList<comercios> filteredList){
        productList = filteredList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ComercioViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewoffer;
        //TextView textViewdir;
        RelativeLayout parentLayout;

        public ComercioViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewoffer = itemView.findViewById(R.id.textViewoffer);
            //textViewdir  = itemView.findViewById(R.id.textViewdir);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

    public void gotoInfoWindow(){

    }
}
