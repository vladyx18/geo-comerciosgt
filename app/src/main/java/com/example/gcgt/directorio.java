package com.example.gcgt;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import ir.mirrajabi.searchdialog.core.Searchable;

public class directorio extends AppCompatActivity {

    List<comercios> productList;

    RecyclerView recyclerView;
    ComercioAdapter mAdapter;
    EditText busc, busc2;
    ImageView img2, img3;
    FloatingActionButton floatb1, floatb2, floatb3, floatb4;
    FloatingActionsMenu floatm;

    private static String URL_DATA= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directorio);

        busc = findViewById(R.id.editText);
        busc2 = findViewById(R.id.editText2);
        img2 = findViewById(R.id.imageView4);
        img3 = findViewById(R.id.imageView5);
        floatb1 = findViewById(R.id.fab_action1);
        floatb2 = findViewById(R.id.fab_action2);
        floatb3 = findViewById(R.id.fab_action3);
        floatb4 = findViewById(R.id.fab_action4);
        floatm = findViewById(R.id.fab_master);

        busc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                  filter(editable.toString());
            }
        });

        busc2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                  filter2(editable.toString());
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clear();
                close();
            }
        });

        floatb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoSearchName();
            }
        });


        floatb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoSearchOffer();
            }
        });

        floatb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(directorio.this, MapsActivity.class);
                startActivity(intent);
            }
        });

        floatb4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(directorio.this, principal.class);
                startActivity(intent);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        productList = new ArrayList<>();

        listing();

    }

    private void clear() {
        busc.setText("");
        busc2.setText("");
    }

    private void filter(String text){
        ArrayList<comercios> filteredList = new ArrayList<>();

        for(comercios item : productList){
            if(item.getTitle().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(item);
            }
        }

        mAdapter.filterList(filteredList);
    }

    private void filter2(String text){
        ArrayList<comercios> filteredList2 = new ArrayList<>();

        for(comercios item2 : productList){
            if(item2.getOferta().toLowerCase().contains(text.toLowerCase())){
                filteredList2.add(item2);
            }
        }

        mAdapter.filterList(filteredList2);
    }

    private void close() {
        busc.setVisibility(View.INVISIBLE);
        busc2.setVisibility(View.INVISIBLE);
        img2.setVisibility(View.INVISIBLE);
        img3.setVisibility(View.INVISIBLE);
        floatm.setVisibility(View.VISIBLE);
    }

    private void listing(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try{
                            JSONObject jobj = new JSONObject(response);
                            JSONArray data = jobj.getJSONArray("data");

                            for(int i =0; i<data.length(); i++) {

                                JSONObject c = data.getJSONObject(i);

                                productList.add(new comercios(
                                        c.getString("cod_comercio"),
                                        c.getString("nombre_comercio"),
                                        c.getString("oferta"),
                                        c.getString("direccion"),
                                        c.getString("correo"),
                                        c.getString("tipo_comercio"),
                                        c.getString("telefono1"),
                                        c.getString("telefono2"),
                                        c.getString("whatsapp1"),
                                        c.getString("telegram1"),
                                        c.getString("whatsapp2"),
                                        c.getString("telegram2"),
                                        c.getString("hora_abierto"),
                                        c.getString("hora_cerrado"),
                                        c.getString("fs_hora_abierto"),
                                        c.getString("fs_hora_cerrado"),
                                        c.getString("altitud"),
                                        c.getString("longitud"),
                                        c.getString("urlimg")

                                ));

                                mAdapter = new ComercioAdapter(directorio.this, productList);
                                recyclerView.setAdapter(mAdapter);
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(directorio.this, "Error!" + e.toString(), Toast.LENGTH_LONG).show();

                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(directorio.this, "Error!" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void gotoSearchName() {
        busc.setVisibility(View.VISIBLE);
        img2.setVisibility(View.VISIBLE);
        img3.setVisibility(View.VISIBLE);
        floatm.setVisibility(View.INVISIBLE);
        floatm.collapse();
    }

    private void gotoSearchOffer() {
        busc2.setVisibility(View.VISIBLE);
        img2.setVisibility(View.VISIBLE);
        img3.setVisibility(View.VISIBLE);
        floatm.setVisibility(View.INVISIBLE);
        floatm.collapse();
    }
}
