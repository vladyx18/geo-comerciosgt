package com.example.gcgt;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class infowindow extends AppCompatActivity implements OnMapReadyCallback{

    TextView nomcomercio, dircomercio, ofercomercio, corrcomercio, tipocomercio, telecomercio1, telecomercio2, horariocomercio, horariocomercio2, cod_comercio;
    ImageView whats1, whats2, tgram1, tgram2, fot;
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    Double alt, lng;
    FloatingActionButton fb1, fb2, fb3;
    String wats1, wats2, tegra1, tegra2;
    int unicode = 0x1F447;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infowindow);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFrag.getMapAsync(this);

        Bundle b1 = getIntent().getExtras();
        final String cod = b1.getString("cod");
        String nombre = b1.getString("nombre");
        String direccion = b1.getString("direccion");
        String oferta = b1.getString("oferta");
        String correo = b1.getString("correo");
        String tipo = b1.getString("tipo");
        final String telefono1 = b1.getString("telefono1");
        final String telefono2 = b1.getString("telefono2");
        String w1 = b1.getString("whatsapp1");
        String t1 = b1.getString("telegram1");
        String w2 = b1.getString("whatsapp2");
        String t2 = b1.getString("telegram2");
        String horab = b1.getString("horab");
        String horac = b1.getString("horac");
        String fs_horab = b1.getString("fs_horab");
        String fs_horac = b1.getString("fs_horac");
        String altitud = b1.getString("altitud");
        String longitud = b1.getString("longitud");
        String urlimg = b1.getString("urlimg");

        final Resources res = getResources();

        alt = Double.parseDouble(altitud);
        lng = Double.parseDouble(longitud);

        cod_comercio = findViewById(R.id.cod_comercio);
        nomcomercio = findViewById(R.id.nombreComercio);
        dircomercio = findViewById(R.id.direccionComercio);
        ofercomercio = findViewById(R.id.ofertaComercio);
        corrcomercio = findViewById(R.id.correoComercio);
        tipocomercio = findViewById(R.id.tipoComercio);
        telecomercio1 = findViewById(R.id.tel1Comercio);
        telecomercio2 = findViewById(R.id.tel2Comercio);
        horariocomercio = findViewById(R.id.horaComercio);
        horariocomercio2 = findViewById(R.id.horaComercio2);
        whats1 = findViewById(R.id.w1);
        tgram1 = findViewById(R.id.t1);
        whats2 = findViewById(R.id.w2);
        tgram2 = findViewById(R.id.t2);
        fb1 = findViewById(R.id.fab_action1);
        fb2 = findViewById(R.id.fab_action2);
        fb3 = findViewById(R.id.fab_action3);
        fot = findViewById(R.id.fotocomer);

        fb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        fb2.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

           }
        });

        fb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if(w1.equals("0")){
            whats1.setVisibility(View.INVISIBLE);
            wats1 = "No";
        }
        if(w1.equals("1")){
            whats1.setVisibility(View.VISIBLE);
            wats1 = "Si";
        }
        if(t1.equals("0")){
            tgram1.setVisibility(View.INVISIBLE);
            tegra1 = "No";
        }
        if(t1.equals("1")){
            tgram1.setVisibility(View.VISIBLE);
            tegra1 = "Si";
        }
        if(w2.equals("0")){
            whats2.setVisibility(View.INVISIBLE);
            wats2 = "No";
        }
        if(w2.equals("1")){
            whats2.setVisibility(View.VISIBLE);
            wats2 = "Si";
        }
        if(t2.equals("0")){
            tgram2.setVisibility(View.INVISIBLE);
            tegra2 = "No";
        }
        if(t2.equals("1")){
            tgram2.setVisibility(View.VISIBLE);
            tegra2 = "Si";
        }

        if(correo.length()==0){
            corrcomercio.setVisibility(View.INVISIBLE);
        }

        if(correo.length()!=0){
            corrcomercio.setVisibility(View.VISIBLE);
        }

        if(telefono2.length()==0){
            telecomercio2.setVisibility(View.INVISIBLE);
        }

        if(telefono2.length()!=0){
            telecomercio2.setVisibility(View.VISIBLE);
        }

        RequestOptions options = new RequestOptions()
                .override(600, 200);

        if(urlimg.equals("null")){
            fot.setImageDrawable(res.getDrawable(R.drawable.logo3));

        }else{
            fot.setImageDrawable(null);
            Glide.with(this).load(urlimg).apply(options).into(fot);
        }

        cod_comercio.setText(cod);
        nomcomercio.setText(nombre);
        dircomercio.setText("Direccion: " + direccion);
        ofercomercio.setText("Oferta: " + oferta);
        corrcomercio.setText("Correo: " + correo);
        tipocomercio.setText("Tipo de Comercio: " + tipo);
        telecomercio1.setText("Tel1: " + telefono1);
        telecomercio2.setText("Tel2: " + telefono2);
        horariocomercio.setText(horab + " a " + horac);
        horariocomercio2.setText(fs_horab + " a " + fs_horac);



    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MarkerOptions markeropts = new MarkerOptions().position(new LatLng(alt, lng));
        markeropts.icon(BitmapDescriptorFactory.fromResource(R.drawable.tienda1));
        googleMap.addMarker(markeropts);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(alt, lng), 15));
        googleMap.getUiSettings().setZoomControlsEnabled(true);

    }
}
