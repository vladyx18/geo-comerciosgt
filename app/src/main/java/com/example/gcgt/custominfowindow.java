package com.example.gcgt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class custominfowindow implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public custominfowindow(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.custominfowindow, null);

        String w1, w2, t1,t2;
        ImageView imgw1 = view.findViewById(R.id.w1);
        ImageView imgw2 = view.findViewById(R.id.w2);
        ImageView imgt1 = view.findViewById(R.id.t1);
        ImageView imgt2 = view.findViewById(R.id.t2);
        TextView txtn = view.findViewById(R.id.nombre);
        TextView txttel1 = view.findViewById(R.id.tel1);
        TextView txttel2 = view.findViewById(R.id.tel2);

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        txttel1.setText("Telefono1: " + infoWindowData.getTel1());
        if(txttel2.length()!=0){
            txttel2.setText("Telefono2: " + infoWindowData.getTel2());
        }
        w1 = infoWindowData.getW1();
        w2 = infoWindowData.getW2();
        t1 = infoWindowData.getT1();
        t2 = infoWindowData.getT2();

        if(w1.equals("0")){
            imgw1.setVisibility(View.INVISIBLE);
        }else if(w1.equals("1")){
            imgw1.setVisibility(View.VISIBLE);
        }

        if(w2.equals("0")){
            imgw2.setVisibility(View.INVISIBLE);
        }else if(w2.equals("1")){
            //imgw2.setVisibility(View.VISIBLE);
        }

        if(t1.equals("0")){
            imgt1.setVisibility(View.INVISIBLE);
        }else if(t1.equals("1")){
            imgt1.setVisibility(View.VISIBLE);
        }

        if(t2.equals("0")){
            imgt2.setVisibility(View.INVISIBLE);
        }else if(t2.equals("1")){
            //imgt2.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
