package com.example.gcgt;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.media.Image;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class registro extends AppCompatActivity {

    private Spinner spntipo;
    private TextView tvlat, tvlng, tverr;
    private EditText comercio, oferta, direccion, correo, tel1, tel2, h1, h2;
    private ImageView w1, w2, t1, t2, info, r1, r2;
    private static String URL_REGIST = "http://everlasting-chemica.000webhostapp.com/reg.php";
    private Button bregis;
    int i = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    String it1, it2;
    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();
    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";
    public final int hora = c.get(Calendar.HOUR_OF_DAY);
    public final int minuto = c.get(Calendar.MINUTE);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        spntipo = findViewById(R.id.spntipo);
        tvlat = findViewById(R.id.tvlat);
        tvlng = findViewById(R.id.tvlong);
        tverr = findViewById(R.id.error);
        comercio = findViewById(R.id.comercio);
        oferta = findViewById(R.id.oferta);
        direccion = findViewById(R.id.direccion);
        correo = findViewById(R.id.correo);
        tel1 = findViewById(R.id.tel1);
        tel2 = findViewById(R.id.tel2);
        w1 = findViewById(R.id.w1);
        w2 = findViewById(R.id.w2);
        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        h1 = findViewById(R.id.hora);
        h2 = findViewById(R.id.hora2);
        r1 = findViewById(R.id.reloj1);
        r2 = findViewById(R.id.reloj2);
        info = findViewById(R.id.info);
        bregis = findViewById(R.id.button);

        tvlat.setEnabled(false);
        tvlng.setEnabled(false);
        h1.setEnabled(false);
        h2.setEnabled(false);
        tverr.setVisibility(View.INVISIBLE);

        final Resources resour = getResources();

        Bundle b = getIntent().getExtras();
        double latitud = b.getDouble("latitud");
        double longitud = b.getDouble("longitud");

        final String total = String.valueOf(latitud);
        final String total2 = String.valueOf(longitud);

        Toast p = Toast.makeText(getApplicationContext(), "Recordatorio: Todos los campos con astericos son obligatorios para el registro!", Toast.LENGTH_LONG);
        p.show();

        String[] Tipos = new String[]{
                "*Seleccione Tipo de Comercio",
                "Restaurante",
                "Panaderia",
                "Pasteleria",
                "Electronicos",
                "Ferreteria",
                "Libreria",
                "Tienda",
                "Venta por Catalogo",
                "Venta de Electrodomesticos",
                "Venta de Celulares",
                "Venta de Videojuegos",
                "Otro",
        };

        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                this, R.layout.spinner_item, Tipos
        );
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spntipo.setAdapter(spinnerArrayAdapter2);

        spntipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent2, View view, int position2, long l) {
                if (parent2.getItemAtPosition(position2).equals("*Seleccione Tipo de Comercio")) {
                    String item2 = parent2.getItemAtPosition(position2).toString();
                    it2 = item2;
                } else {
                    String item2 = parent2.getItemAtPosition(position2).toString();
                    //Toast.makeText(parent2.getContext(), "Selecciono " + item2, Toast.LENGTH_SHORT).show();
                    it2 = item2;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

                //TODO Auto-generated method stub
            }
        });

        tvlat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvlat.setText(total);
            }
        });

        tvlng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvlng.setText(total2);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tvlat.performClick();
                tvlng.performClick();
            }
        }, 1000);

        w1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i == 1) {
                    i = 0;
                    w1.setImageDrawable(resour.getDrawable(R.drawable.whatsapp2));
                } else if (i == 0 && tel1.length() > 7) {
                    i = 1;
                    w1.setImageDrawable(resour.getDrawable(R.drawable.whatsapp));
                }
            }
        });

        w2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i2 == 1) {
                    i2 = 0;
                    w2.setImageDrawable(resour.getDrawable(R.drawable.whatsapp2));
                } else if (i2 == 0 && tel2.length() > 7) {
                    i2 = 1;
                    w2.setImageDrawable(resour.getDrawable(R.drawable.whatsapp));
                }
            }
        });

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i3 == 1) {
                    i3 = 0;
                    t1.setImageDrawable(resour.getDrawable(R.drawable.telegram2));
                } else if (i3 == 0 && tel1.length() > 7) {
                    i3 = 1;
                    t1.setImageDrawable(resour.getDrawable(R.drawable.telegram));
                }
            }
        });

        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i4 == 1) {
                    i4 = 0;
                    t2.setImageDrawable(resour.getDrawable(R.drawable.telegram2));
                } else if (i4 == 0 && tel2.length() > 7) {
                    i4 = 1;
                    t2.setImageDrawable(resour.getDrawable(R.drawable.telegram));
                }
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast t = Toast.makeText(getApplicationContext(), "Usted puede seleccionar si tiene whatsapp, telegram o ambos en el numero que esta registrando haciendo click en las imagenes. Si esta a color la imagen significa tiene, Si esta en blanco y negro significara no tiene", Toast.LENGTH_LONG);
                t.show();


            }
        });

        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerHora();
            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                obtenerHora2();
            }
        });

       bregis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(comercio.length()==0){
                   comercio.setError("Ingrese el nombre de su comercio!");
               }
               else if(direccion.length()==0){
                   direccion.setError("Ingrese la direccion de su comercio!");
               }
               else if(oferta.length()==0){
                   oferta.setError("Ingrese la oferta de su comercio!");
               }
               else if(tel1.length()==0){
                   tel1.setError("Ingrese el numero de telefono de su comercio!");
               }
               else if(tel1.length() < 8){
                   tel1.setError("El numero que ingreso no es valido, porfavor ingrese un numero valido!");
               }
               else if(tel2.length() > 0) {
                   if (tel2.length() < 8) {
                       tel2.setError("El numero que ingreso no es valido, porfavor ingrese un numero valido!");
                   }
               }
               else if(h1.length() == 0) {
                   r1.setImageDrawable(resour.getDrawable(R.drawable.clock_er));
                   Toast error = Toast.makeText(getApplicationContext(), "No ingreso el horario de apertura de su comercio!", Toast.LENGTH_SHORT);
                   error.show();
               }
               else if(h1.length() != 0) {
                   r1.setImageDrawable(resour.getDrawable(R.drawable.clock));
                   if (h2.length() == 0) {
                       r2.setImageDrawable(resour.getDrawable(R.drawable.clock_er));
                       Toast error = Toast.makeText(getApplicationContext(), "No ingreso el horario de cierre de su comercio!", Toast.LENGTH_SHORT);
                       error.show();
                   }
                   else if(h2.length() != 0) {
                       r2.setImageDrawable(resour.getDrawable(R.drawable.clock));
                       if (it2.equals("*Seleccione Tipo de Comercio")) {
                           tverr.setVisibility(View.VISIBLE);
                           tverr.setError("Seleccion incorrecta, elija un tipo de comercio!");
                           Toast te = Toast.makeText(getApplicationContext(), "Seleccion incorrecta, elija un tipo de comercio!", Toast.LENGTH_SHORT);
                           te.show();
                       }
                       else {
                           tverr.setVisibility(View.INVISIBLE);
                           //Toast te = Toast.makeText(getApplicationContext(), "Registrando datos...", Toast.LENGTH_SHORT);
                           //te.show();
                           Regist();
                       }
                   }
               }
            }
        });
    }

    private void Regist() {
        final String comercio = this.comercio.getText().toString().trim();
        final String oferta = this.oferta.getText().toString().trim();
        final String direccion = this.direccion.getText().toString().trim();
        final String correo = this.correo.getText().toString().trim();
        final String telef1 = this.tel1.getText().toString().trim();
        final String w1 = String.valueOf(i).trim();
        final String t1 = String.valueOf(i3).trim();
        final String telef2 = this.tel2.getText().toString().trim();
        final String w2 = String.valueOf(i2).trim();
        final String t2 = String.valueOf(i4).trim();
        final String horab = this.h1.getText().toString().trim();
        final String horac = this.h2.getText().toString().trim();
        final String tipo = this.it2.trim();
        final String al = this.tvlat.getText().toString().trim();
        final String log = this.tvlng.getText().toString().trim();


        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL_REGIST,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("1")) {
                                Toast.makeText(registro.this, "Registro completado!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(registro.this, MapsActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(registro.this, "Error de registro!" + e.toString(), Toast.LENGTH_LONG).show();

                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(registro.this, "Error de registro!" + error.toString(), Toast.LENGTH_LONG).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("comercio", comercio);
                params.put("direccion", direccion);
                params.put("oferta", oferta);
                params.put("correo", correo);
                params.put("tel1", telef1);
                params.put("w1", w1);
                params.put("t1", t1);
                params.put("tel2", telef2);
                params.put("w2", w2);
                params.put("t2", t2);
                params.put("horab", horab);
                params.put("horac", horac);
                params.put("tipo", tipo);
                params.put("altitud", al);
                params.put("longitud", log);
                return params;

            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void obtenerHora(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                h1.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }

    private void obtenerHora2(){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                h2.setText(horaFormateada + DOS_PUNTOS + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }
}
